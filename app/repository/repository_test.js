'use strict';

describe('myApp.searchmodule', function() {

  beforeEach(module('myApp.repository'));

  describe('repository controller', function(){

    it('should display repository results from Github', inject(function($controller) {
      //spec body
      var repositoryCtrl = $controller('RepositoryCtrl');
      expect(repositoryCtrl).toBeDefined();
    }));

  });
});

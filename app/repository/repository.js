'use strict';

angular.module('myApp.repository', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/repositories', {
    templateUrl: 'repository/repository.html',
    controller: 'RepositoryCtrl'
  });
}])

.controller('RepositoryCtrl', [function() {

  // GET request to GitHub :
  $http.get('https://api.github.com/').
    then(function(response) {
      // this callback will be called asynchronously
      // when the response is available
    }, function(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });

}]);

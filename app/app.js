'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.repository',
  'myApp.view2',
  'myApp.version',
  'ui.bootstrap'
])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.otherwise({redirectTo: '/repositories'});
}])
.factory('repositoriesFactory', function($http){
  var factory = {};
  
  factory.findRepositories = function($scope) {
    // Simple GET request example :
    $http.get('https://api.github.com/search/repositories?q='.$scope.searchTerm).
      then(function(response) {
        return = response.data;
      });
    }
    
    return factory;
})
.controller('myController', function($scope, repositoriesFactory) {
  $scope.repositories = {};
  
  $scope.search = function() {
      $scope.repositories = repositoriesFactory.findRepositories($scope);
    };
  
  });
